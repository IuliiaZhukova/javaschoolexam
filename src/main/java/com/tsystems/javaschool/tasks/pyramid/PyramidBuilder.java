package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Checking for emptiness
        if( inputNumbers == null || inputNumbers.size()==0){
            throw new CannotBuildPyramidException();
        }
        // Sorting
        try {
            Collections.sort(inputNumbers);
        }
        catch ( NullPointerException | OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }

        // Checking the possibility of building a pyramid
        double d = Math.sqrt(1+2*4*inputNumbers.size());
        double high = (-1 + d)/2;
        double width = 2*high - 1;

        if(d != (int)d && high != (int)high && width != (int)width){
            throw new CannotBuildPyramidException();
        }
        // Creating array
        int[][] pyramid = new int[(int)high][(int)width];

        // Filling array
        int centre = (int)width/2;
        int count = 1;
        int index = 0;
        for (int i = 0; i < pyramid.length; i++, count++) {
            for (int j = 0; j <count*2; j += 2) {
                pyramid[i][centre+j] = inputNumbers.get(index++);
            }
            centre--;
        }

        return pyramid;
    }


}
