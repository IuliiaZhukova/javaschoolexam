package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // Checking for emptiness
        if( x == null || y == null){
            throw new IllegalArgumentException();
        }

        if( x.size()==0){
            return true;
        }

        //  Checking for possibilities to get a sequence which is equal to the first
        List copyY = new ArrayList();
        copyY.addAll(y);
        if (copyY.retainAll(x)) {
            List removedDuplicate = (List) copyY.stream().distinct().collect(Collectors.toList());
            for (int i = 0; i < removedDuplicate.size(); i++) {
                return removedDuplicate.get(i).equals(x.get(i)) && removedDuplicate.size() == x.size();
            }
        }
        return false;
    }
}
