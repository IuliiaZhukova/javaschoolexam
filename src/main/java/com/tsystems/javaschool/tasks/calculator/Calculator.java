package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        return format(execute(parser(statement)));
    }

    public static String format(Double statement){
        // Checking for emptiness
        if(statement == null){
            return null;
        }
        // Formatting
        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        return decimalFormat.format(statement);
    }

    public static Double execute(List<Character> list)  {
        // Checking for emptiness
        if(list == null){
            return null;
        }
        // Array to String
        String str = "";
        for (Character ch :list) {
            str += ch;
        }
        // Execution operations
        Stack<Double> stack = new Stack<>();
        for (String c : str.split(" ")) {
            if(!c.matches("[-/+*]?")) {
                stack.add(Double.parseDouble(c));
            }
            else {
                switch (c){
                    case "+":{
                        stack.add(stack.pop() + stack.pop());
                        break;
                    }
                    case "-":{
                        stack.add(- stack.pop() + stack.pop());
                        break;
                    }
                    case "*":{
                        stack.add(stack.pop() * stack.pop());
                        break;
                    }
                    case "/":{
                        double num2 = stack.pop();
                        double num1 = stack.pop();
                        if( num1 == 0 || num2 == 0){
                            return null;
                        }
                        stack.add(num1/num2);
                        break;
                    }

                    default:
                        break;
                }
            }
        }

        return stack.pop();
    }

    // Reverse Polish notation
    public static List<Character> parser(String s) {
        if(s == null || s.isEmpty()){
            return null;
        }
        List<Character> postfix = new ArrayList<>();
        Stack<Character> stack = new Stack<>();
        char previousStep = ' ';
        char[] ch = s.toCharArray();
        for (Character c: ch) {
            // c == 0-9, .
            if(c > 47 & c < 58 | c == 46){
                if(previousStep == 46 & c ==46){
                    return null;
                }
                postfix.add(c);
            }
            // Repetition check
            else if( c != 40 && (previousStep == 40 | previousStep == 45 | previousStep == 43 | previousStep == 42
                    | previousStep == 47 )){
                return null;
            }
            // c == -, +, *, /
            else if ( c == 45 | c == 43 | c == 42 | c == 47){
                if( c == 45 & stack.isEmpty() & postfix.isEmpty()){
                    postfix.add(c);
                }
                else {
                    postfix.add(' ');
                    if (!stack.isEmpty() && priority(c) <= priority(stack.peek())) {
                        postfix.add(stack.pop());
                        postfix.add(' ');
                    }
                    stack.add(c);
                }
            }
            // c == "("
            else if (c == 40  ){
                postfix.add(' ');
                stack.add(c);
            }
            // c == )
            else if (c == 41){
                if(previousStep == 41){
                    return null;
                }
                while ( stack.peek() != 40){
                    if(stack.isEmpty()){
                        return null;
                    }
                    postfix.add(' ');
                    postfix.add(stack.pop());
                }
                stack.pop();
            }
            else {
                return null;
            }
            previousStep = c;
        }
        while ( !stack.isEmpty()){
            postfix.add(' ');
            postfix.add(stack.pop());
            if(postfix.get(postfix.size()-1) == 40 ){
                return null;
            }
        }

        return postfix;
    }

    private static int priority(Character token) {
        if (token == '(') return 1;
        if (token == '+' || token == '-') return 2;
        if (token == '*' || token == '/') return 3;
        return 4;
    }

}
